using Zenject;

namespace AttaboysGames.Package.Runtime
{
	public static class ZenjectExtensions
	{
		public static void BindDatabase<TDatabase>(this DiContainer container, TDatabase database)
		{
			container.BindInterfacesTo<TDatabase>().FromInstance(database).AsSingle();

			var type = typeof(TDatabase);
			
			if(type.GetInterface(nameof(IInitializable)) == null)
				return;

			container.BindInitializableExecutionOrder(type, -5000);
		}
		
		public static void BindDaoStorage<TStorage, TStorageDao>(this DiContainer container, string storageName)
		{
			container.BindInterfacesTo<TStorage>().AsSingle();
			container.BindInterfacesTo<TStorageDao>().AsSingle().WithArguments(storageName);
		}
	}
}